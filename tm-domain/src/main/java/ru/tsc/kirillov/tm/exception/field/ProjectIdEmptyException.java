package ru.tsc.kirillov.tm.exception.field;

public final class ProjectIdEmptyException extends IdEmptyException {

    public ProjectIdEmptyException() {
        super("Ошибка! ID проекта не задано.");
    }

}
