package ru.tsc.kirillov.tm.exception.entity;

import lombok.NoArgsConstructor;
import ru.tsc.kirillov.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractEntityException extends AbstractException {

    public AbstractEntityException(final String message) {
        super(message);
    }

    public AbstractEntityException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(final Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(
            final String message,
            final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
